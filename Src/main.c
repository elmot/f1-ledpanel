/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  * COPYRIGHT(c) 2016 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"
#include "usb_device.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
TIM_HandleTypeDef htim1;
DMA_HandleTypeDef hdma_tim1_up;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
#define WIDTH 37
#define HEIGHT 8

#define RESET_ZERO_COUNT 200
#define LED_BYTES (24 *3)
#define BYTES (RESET_ZERO_COUNT + WIDTH * LED_BYTES * 2)

#define SNOW_H_COUNT 3
#define SNOW_COLOR 0x101010

//uint32_t marker [] = {0xFFFFFF,0x3F3F3F,0x0F0F0F, 0x030303}; 
uint32_t marker1 [] = {0x000003,0x00000f,0x00003f,0x00007f,0x0000ff,
	0x000300,0x000f00,0x003f00,0x007f00,0x00ff00,
	0x030000,0x0f0000,0x3f0000,0x7f0000,0xff0000,
	0x030300,0x0f0f00,0x3f3f00,0x7f7f00,0xffff00,
}; 
uint32_t marker[] = {
		0xC71585,0xFF1493, 0xFFC0CB,  
	
	0x000003,0x00000f,0x00003f,0x00007f,0x0000ff,
	0x000300,0x000f00,0x003f00,0x007f00,0x00ff00,
	0x030000,0x0f0000,0x3f0000,0x7f0000,0xff0000,
	0x030300,0x0f0f00,0x3f3f00,0x7f7f00,0xffff00,
}; 

const uint8_t BIT_MASKS[] ={1, 1, 2, 2, 4, 4, 8, 8};
const uint8_t BIT_MASKS_HIGH[] ={16, 16, 32, 32, 64, 64, 128, 128};

const uint32_t led_offset [] = {
	/*g*/ 70, 67, 64, 61, 58, 55, 52, 49,
	/*b*/ 46, 43, 40, 37, 34, 31, 28, 25,
	/*r*/ 22, 19, 16, 13, 10,  7,  4,  1,
}; 
#define colorTableLen 36
const unsigned int colorTable[] = {
		0x661414 ,
		0x662114 ,
		0x662F14 ,
		0x663D14 ,
		0x664A14 ,
		0x665814 ,
		0x666614 ,
		0x586614 ,
		0x4A6614 ,
		0x3D6614 ,
		0x2F6614 ,
		0x216614 ,
		0x146614 ,
		0x146621 ,
		0x14662F ,
		0x14663D ,
		0x14664A ,
		0x146658 ,
		0x146666 ,
		0x145866 ,
		0x144A66 ,
		0x143D66 ,
		0x142F66 ,
		0x142166 ,
		0x141466 ,
		0x211466 ,
		0x2F1466 ,
		0x3D1466 ,
		0x4A1466 ,
		0x581466 ,
		0x661466 ,
		0x661458 ,
		0x66144A ,
		0x66143D ,
		0x66142F ,
		0x661421 ,
};

volatile int8_t softFrame = 0;
volatile uint32_t periodEnds = 0;
static uint8_t screenBuffer[BYTES];
static uint8_t softBuffer[BYTES];
int effectShift = 0;

extern const unsigned int * screens[];
extern const int screenCount;


/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_TIM1_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

void pointOr(int x, int y, uint32_t color)
{
		uint8_t bitMask = BIT_MASKS[y];
		int bitShift;
		if(y % 2 ) {
			bitShift = 72 * (WIDTH + x); 
		} else {
			bitShift = 72 * (WIDTH - x - 1); 
		}
		for(int k = 0; k < 24; k++)
		{
			int index = 200 + bitShift + k*3 + 1;
			if(color & 0x800000) {
				screenBuffer [index] |= bitMask;
			}
			color <<=1;
		}

}

void fillBufferFromPicture(const unsigned int* picture,
		unsigned int (*colorTransform)(int x , int y, unsigned int color))
{
	for(int y =0; y < HEIGHT; y++){
		uint8_t bitMask = BIT_MASKS[y];
		uint8_t negBitMask = ~ bitMask;
		for(int x =0; !softFrame && x < WIDTH; x++) {
			int bitShift;
			if(y % 2 ) {
				bitShift = 72 * (WIDTH + x); 
			} else {
				bitShift = 72 * (WIDTH - x - 1); 
			}
			int color  = picture [ y * WIDTH + x];
			if(colorTransform != NULL) color = colorTransform(x, y,color);
			for(int k = 0;!softFrame && k < 24; k++)
			{
				int index = 200 + bitShift + k*3 + 1;
				if(color & 0x800000) {
					screenBuffer [index] |= bitMask;
				} else {
					screenBuffer [index] &= negBitMask;
				}
				color <<=1;
			}

		}

	}
}


void delay(int time) {
	for(int till = time + HAL_GetTick(); till >= HAL_GetTick(); __wfi()); 
}


void startPeriod(int ms)
{
  periodEnds = ms + HAL_GetTick();
}
void waitForPeriod()
{
	while(HAL_GetTick()< periodEnds);
}

unsigned int effect(int x, int y, unsigned int color)
{
	if(color == 0xFEFEFE) {
		return colorTable[(x + y + effectShift) % colorTableLen];
	}
	else {
		return color;
	}
}

int snowIdx = 0;
int snowData[HEIGHT ][SNOW_H_COUNT];

void snow(int step)
{
	for(int i = 0;!softFrame &&  i < HEIGHT; i++)
	{
		int * snowLine = snowData[(i + snowIdx) % HEIGHT];
		for(int j = 0; j < SNOW_H_COUNT; j++)
		{
			pointOr(snowLine[j], i, SNOW_COLOR);

		}
	}
	if(step) {
		snowIdx = (snowIdx + HEIGHT - 1) % HEIGHT;
		for(int i = 0; i < SNOW_H_COUNT; i++)
		{
			snowData[snowIdx][i] = rand() % WIDTH;
		}
	}
}

volatile int framePointer = 0;

void resetPointers()
{
	framePointer = 0;
}

void receiveChar(char c)
{
	{
		switch(c)
		{
		case 'z':
		case 'Z':
			softFrame = -1;
			return;
		case 'r':
		case 'R':
			resetPointers();
			return;
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9': c -= '0'; break;
		case 'a':
		case 'b':
		case 'c':
		case 'd':
		case 'e':
		case 'f': c += -'a' + 10; break;
		case 'A':
		case 'B':
		case 'C':
		case 'D':
		case 'E':
		case 'F': c += -'A' + 10; break;
		default: return;
		}
		uint8_t y = framePointer / 6 / WIDTH;
		uint8_t x = (framePointer / 6) % WIDTH;
		uint8_t bitMask = BIT_MASKS_HIGH[y];
		uint8_t negBitMask = ~ bitMask;
		int bitShift;
		
		if(y % 2) bitShift = 72 * (WIDTH + x); else bitShift = 72 * (WIDTH - x - 1); 
		bitShift += 12 * (framePointer % 6);
		for(int k = 0; k < 12; k += 3)
		{
			int index = RESET_ZERO_COUNT + bitShift + k + 1;
			if(c & 0x8) {
				softBuffer [index] |= bitMask;
			} else {
				softBuffer [index] &= negBitMask;
			}
			c <<=1;
		}
		framePointer = (framePointer + 1) % (WIDTH * HEIGHT * 6);
		if(framePointer == 0)
		{
			softFrame = 1;
			for (int i = RESET_ZERO_COUNT + 1; i < BYTES; i += 3)
			{
				softBuffer[i] >>= 4;
			}
		}
	}

}

void resetBuffer(uint8_t *buffer)
{
	for(int i = 0; i < RESET_ZERO_COUNT; i++)
		buffer[i] = 0;
	for(int i = RESET_ZERO_COUNT; i < BYTES; i+=3)
	{
			buffer[i] = 0xff;
			buffer[i+1] = 0x0;
			buffer[i+2] = 0x0;
	}
}
/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */
	//Simulate USB disconnect
	{
	  GPIO_InitTypeDef GPIO_InitStruct;

		__HAL_RCC_GPIOA_CLK_ENABLE();
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11|GPIO_PIN_12, GPIO_PIN_RESET);

		/*Configure GPIO pins : PA11 PA12 */
		GPIO_InitStruct.Pin = GPIO_PIN_11|GPIO_PIN_12;
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
		GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
		HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
		for(long l = 0; l <5000000;l++)
		 __nop();
	}
	
  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_TIM1_Init();
  MX_USB_DEVICE_Init();

  /* USER CODE BEGIN 2 */
	
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	while (1)
  {
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */
		softFrame = 0;
		delay(20);
		
		/* reset buffer*/
		resetBuffer(screenBuffer);
		resetBuffer(softBuffer);
		
		__HAL_TIM_ENABLE_DMA(&htim1, TIM_DMA_UPDATE);
		HAL_TIM_Base_Start(&htim1);

		for (unsigned int screenIdx = 0; !softFrame ; screenIdx++)
	 {
		startPeriod(50);
		fillBufferFromPicture(screens[screenIdx / 300 % screenCount], effect);
//		snow(effectShift % 10 == 0);
		effectShift++;
		HAL_DMA_Start(&hdma_tim1_up, (uint32_t)screenBuffer, 1 + (uint32_t)&GPIOB->ODR, BYTES);
		HAL_DMA_PollForTransfer(&hdma_tim1_up,HAL_DMA_FULL_TRANSFER,20000);
		waitForPeriod();
	 }
	 
		while(softFrame != -1) {
			if(softFrame != -1)	softFrame = 0;

			HAL_DMA_Start(&hdma_tim1_up, (uint32_t)softBuffer, 1 + (uint32_t)&GPIOB->ODR, BYTES);
			HAL_DMA_PollForTransfer(&hdma_tim1_up,HAL_DMA_FULL_TRANSFER,20000);
		}
  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  HAL_RCC_OscConfig(&RCC_OscInitStruct);

  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
  HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2);

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USB;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_PLL_DIV1_5;
  HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit);

  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* TIM1 init function */
void MX_TIM1_Init(void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;

  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 0;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 23;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  HAL_TIM_Base_Init(&htim1);

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig);

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig);

}

/** 
  * Enable DMA controller clock
  */
void MX_DMA_Init(void) 
{
  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel5_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel5_IRQn);

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10|GPIO_PIN_11|GPIO_PIN_8|GPIO_PIN_9, GPIO_PIN_RESET);

  /*Configure GPIO pins : PB10 PB11 PB8 PB9 */
  GPIO_InitStruct.Pin = GPIO_PIN_10|GPIO_PIN_11|GPIO_PIN_8|GPIO_PIN_9;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
  GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
