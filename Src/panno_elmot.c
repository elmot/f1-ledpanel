
#define Z 0xFEFEFE,

#define b 0x000001,
#define Y 0x403200,
#define y 0x070501,
#define P 0x200020,
#define G 0x001000,
#define V 0X030323,
#define R 0X300000,
#define W 0X404040,
#define B 0X000000,
#define p 0X803030,

#define F 0x010000,
#define k 0x00000f,

const unsigned int panno1 [] = {
//0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6
  B B G G G G G G B Y B B B B B B B B B B B B B B B B B B B B R B B B B B B 
  B B G G B B B B B Y B B B B B B B B B B B B B B B B B B B B R B B B B B B 
  B B G G B B B B B Y B B B B B V B B B B B B B B B B B B B R R R B B B B B 
  B B G G G G G B B Y B B B B B V V V B V V B B B Z Z Z B B B R B B B B B B 
  B B G G B B B B B Y B B B B B V B B V B B V B Z B B B Z B B R B B B B B B 
  B B G G B B B B B Y B B B B B V B B V B B V B Z B B B Z B B R B B B B B B 
  B B G G B B B B B Y B B B Y B V B B V B B V B Z B B B Z B B R B B R B B B 
  B B G G G G G G B B Y Y Y B B V B B V B B V B B Z Z Z B B B B R R B B B B 
};

const unsigned int panno2a [] = {
//0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6
  F F F F Z Z F F F F F F F F Z Z Z Z Z Z F F F F Z Z Z Z Z Z F F F F F F F
  F F F F Z Z F F F F F F F F Z Z F F F F F F F F Z Z F F F Z Z F F F F F F
  F F F F Z Z F F F F F F F F Z Z F F F F F F F F Z Z F F F Z Z F F F F F F
  F F F F Z Z F F F F F F F F Z Z Z Z Z F F F F F Z Z F F F Z Z F F F F F F
  F F F F Z Z F F F F F F F F Z Z F F F F F F F F Z Z F F F Z Z F F F F F F
  F F F F Z Z F F F F F F F F Z Z F F F F F F F F Z Z F F F Z Z F F F F F F
  F F F F Z Z Z Z Z Z F F F F Z Z F F F F F F F F Z Z F F F Z Z F F F F F F
  F F F F Z Z Z Z Z Z F F F F Z Z Z Z Z Z F F F F Z Z Z Z Z Z F F F F F F F
};

const unsigned int * screens [] = { panno1, panno2a};
const int screenCount  = 2;


