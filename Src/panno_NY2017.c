
#define Z 0xFEFEFE,

#define b 0x000001,
#define Y 0x403200,
#define y 0x070501,
#define P 0x200020,
#define G 0x001000,
#define V 0X030323,
#define R 0X300000,
#define W 0X404040,
#define B 0X000000,
#define p 0X803030,

#define F 0x010000,
#define k 0x00000f,

const unsigned int panno1 [] = {
//0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6
  b Z Z Z b b b Z Z Z b b b b Z b b b Z Z Z Z Z b b b b b b R b b b b b b b
  Z b b b Z b Z b b b Z b b Z Z b b b b b b b Z b b b b b b G b b b b b b b
  b b b b Z b Z b b b Z b Z b Z b b b b b b Z b b b b b b G G G b b b b b b
  b b b Z b b Z b b b Z b b b Z b b b b b Z b b b b b b G P G P G b b b b b
  b b Z b b b Z b b b Z b b b Z b b b b Z b b b b b b G V G V G V G b b b b
  b Z b b b b Z b b b Z b b b Z b b b b Z b b b b b b G G Y G Y G G b b b b
  Z b b b b b Z b b b Z b b b Z b b b b Z b b b b b G G G G G G G G G b b b
  Z Z Z Z Z b b Z Z Z b b Z Z Z Z Z b b Z b b b b b b b b y y y b b b b b b
};

const unsigned int panno2a [] = {
//0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6
  Z Z b b Z Z b b b Z Z b b b Z Z Z Z Z b b Z Z Z Z Z b b Z Z b b b b Z Z b
  Z Z b b Z Z b b Z Z Z Z b b Z Z b b Z Z b Z Z b b Z Z b b Z Z b b Z Z b b
  Z Z b b Z Z b Z Z b b Z Z b Z Z b b Z Z b Z Z b b Z Z b b b Z Z Z Z b b b
  Z Z Z Z Z Z b Z Z b b Z Z b Z Z b b Z Z b Z Z b b Z Z b b b b Z Z b b b b
  Z Z b b Z Z b Z Z Z Z Z Z b Z Z Z Z Z b b Z Z Z Z Z b b b b b Z Z b b b b
  Z Z b b Z Z b Z Z b b Z Z b Z Z b b b b b Z Z b b b b b b b b Z Z b b b b
  Z Z b b Z Z b Z Z b b Z Z b Z Z b b b b b Z Z b b b b b b b b Z Z b b b b
  Z Z b b Z Z b Z Z b b Z Z b Z Z b b b b b Z Z b b b b b b b b Z Z b b b b
};

const unsigned int panno2b[] = {
//0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6
  b b b b Z Z b b b Z Z b Z Z Z Z Z Z b Z Z b b b b b b b b Z Z b b b b b b
  b b b b Z Z b b b Z Z b Z Z b b b b b Z Z b b b b b b b b Z Z b b b b b b
  b b b b Z Z Z b b Z Z b Z Z b b b b b Z Z b b b Z Z b b b Z Z b b b b b b
  b b b b Z Z Z Z b Z Z b Z Z Z Z Z b b Z Z b b Z Z Z Z b b Z Z b b b b b b
  b b b b Z Z b Z Z Z Z b Z Z b b b b b Z Z b b Z b b Z b b Z Z b b b b b b
  b b b b Z Z b b Z Z Z b Z Z b b b b b b Z Z Z Z b b Z Z Z Z b b b b b b b
  b b b b Z Z b b b Z Z b Z Z b b b b b b b Z Z b b b b Z Z b b b b b b b b
  b b b b Z Z b b b Z Z b Z Z Z Z Z Z b b b Z Z b b b b Z Z b b b b b b b b
};

const unsigned int panno2c[] = {
//0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6
  b Z Z b b b b Z Z b Z Z Z Z Z Z b b b Z Z b b b b Z Z Z Z Z b b b b Z b b
  b b Z Z b b Z Z b b Z Z b b b b b b Z Z Z Z b b b Z Z b b Z Z b b Z Z Z b
  b b b Z Z Z Z b b b Z Z b b b b b Z Z b b Z Z b b Z Z b b Z Z b b Z Z Z b
  b b b b Z Z b b b b Z Z Z Z Z b b Z Z b b Z Z b b Z Z b b Z Z b b b Z b b
  b b b b Z Z b b b b Z Z b b b b b Z Z Z Z Z Z b b Z Z Z Z Z b b b b b b b
  b b b b Z Z b b b b Z Z b b b b b Z Z b b Z Z b b Z Z b Z Z b b b b Z b b
  b b b b Z Z b b b b Z Z b b b b b Z Z b b Z Z b b Z Z b b Z Z b b Z Z Z b
  b b b b Z Z b b b b Z Z Z Z Z Z b Z Z b b Z Z b b Z Z b b Z Z b b b Z b b
};

const unsigned int panno3 [] = {
//0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6
  b b b b b b b b Y Y Y b b b b R R R R R W b b b b b b b b R b b b b b b b
  b b b b b b b Y Y Y Y Y b b R R R R R b b b b b b b b b b G b b b b b b b
  b b b Y Y Y Y Y Y b Y Y b b W W W W W b b b b b b b b b G G G b b b b b b
  b b Y Y Y Y Y Y Y b b b b b p 18,p 18,p b b b b b b b b G P G P G b b b b b
  b Y Y Y Y Y Y Y Y b b b b b p p p p p b b b b b b b G V G V G V G b b b b
  Y b Y b b b b b Y b b b b b W W W W W b b b b b b b G G Y G Y G G b b b b
  b b Y b b b b b Y b b b b b W W W W W b b b b b b G G G G G G G G G b b b
  b b Y b b b b b Y b b b b b b W W W b b b b b b b b b b y y y b b b b b b
};

const unsigned int * screens [] = { panno1, panno2a, panno2b, panno2c, panno3};
const int screenCount  = 5;


